import java.awt.Color;
import java.awt.Container;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.Graphics;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
public class Game extends JFrame implements KeyListener, Runnable,
		ActionListener {

	private MyPanel pane; // game pane
	private ArrayList<Squart> squart; // Store the squart in the pane
	private Squart car; // A red cube represent a car
	private int speed; // car speed
	private int mileage;
	private Menu menu = new Menu("Menu"); // Game menu
	private MenuItem restart = new MenuItem("New Game"); // restart a game
	private MenuItem rank = new MenuItem("Best score"); // check the Best score
	private MenuItem exit = new MenuItem("Exit"); // Exit the game
	private MenuBar bar = new MenuBar();
	private ArrayList<Player> rankScore; // Rank Score
   private int diffcultly; // The diffcultly level of the game
	private boolean pause = false; // To check if the game is pause or not
	public Game() throws Exception {
		// TODO Auto-generated constructor stub
		rankScore = new ArrayList<Player>();

		File file = new File("rank.dat");
		if (!file.exists()) // If the file dosen't exist create one
			file.createNewFile();
		else {
			BufferedReader is = new BufferedReader(new FileReader("rank.dat"));
			String line;
			Player player;
			while ((line = is.readLine()) != null){
				player = new Player();
				player.name = line;
				line = is.readLine();
				player.score = Integer.valueOf(line);
				rankScore.add(player);
			}
			is.close();
		}

		this.squart = new ArrayList<Squart>();
		this.car = new Squart(70, 260);
		this.car.color = Color.red;
		this.speed = 800;
		this.mileage = 0;

		menu.add(restart);
		restart.addActionListener(this);
		menu.add(rank);
		rank.addActionListener(this);
		menu.addSeparator();
		menu.add(exit);
		exit.addActionListener(this);
		bar.add(menu);
		this.setMenuBar(bar);
		pane = new MyPanel();
		pane.state.setText("Status: Running ");
		pane.car = car;
		pane.squart = squart;
		pane.addKeyListener(this);
		this.addKeyListener(this);
		this.add(pane);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setSize(400, 340);
		this.show();
		this.setResizable(false);
	}

	public static void main(String[] args) throws Exception {
		new Thread(new Game()).start();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		boolean flag = true;
		while (true) {
			try {
				if (pause)
					this.wait();
				runing();
				if (flag) {
					produce();
					flag = false;
				} else
					flag = true;
				pane.repaint();
				Thread.sleep(this.speed);
			} catch (Exception e) {
			}
		}

	}

	// Restart game
	public void restart() {
		this.squart = new ArrayList<Squart>();
		this.car = new Squart(70, 260);
		this.car.color = Color.red;
		this.speed = 800;
		this.mileage = 0;
		pane.car = car;
		pane.squart = squart;
	}

	// Random produce squart
	public void produce() {
      if (diffcultly == 0) {
			int name0 = Integer.parseInt(JOptionPane.showInputDialog("Please select the diffcultly (1.Easy, 2.Medium, 3.Hard)"));
			diffcultly = name0;
         }
		Random rand = new Random();
		Squart s;
      int num=0;
		for (int i = 0; i < 5&& num<4; i++) {
			if (Math.abs(rand.nextInt() % 10) < diffcultly) {
				s = new Squart(i * 30+10, 10);
				squart.add(s);
            num++;
			}
		}
	}

	// squart move down
	public void runing() throws Exception {
		synchronized (this) {
			for (int i = 0; i < squart.size(); i++) {
				squart.get(i).y += 25;
				if (squart.get(i).y == 260)
					if (squart.get(i).x == car.x)
						gameOver();
				if (squart.get(i).y > 260){// remove squart in the bottom 
						squart.remove(i);
						i--;
					}
				}
			this.mileage += 25;
			pane.mileage.setText("mileage: " + this.mileage);
			switch (this.mileage) {
         case 300:
				if (this.speed > 500)
					this.speed = 500;
				break;
			case 600:
				if (this.speed > 400)
					this.speed = 400;
				break;
			case 1200:
				if (this.speed > 300)
					this.speed = 300;
				break;
			case 2000:
				if (this.speed > 200)
					this.speed = 200;
				break;
         case 3000:
				if (this.speed > 150)
					this.speed = 150;
				break;
         case 4000:
				if (this.speed > 100)
					this.speed = 100;
				break;
			case 10000:
				if (this.speed > 75)
					this.speed = 75;
				break;
         case 50000:
				if (this.speed > 50)
					this.speed = 50;
				break;
			}
			pane.speed.setText("Speed: " + (1000/this.speed)*20);
		}
	}

	//Game Over 
	public void gameOver() throws InterruptedException, IOException {
		if (rankScore.size() == 0) {
			String name = JOptionPane.showInputDialog("Score: " + this.mileage
					+ " Congratulation!!\nPlease Enter your name: ");
			rankScore.add(new Player(name, this.mileage));
         diffcultly = 0;
			restart();
			pane.repaint();
			return;
		}
		for (int i = 0; i < rankScore.size(); i++)
			if (this.mileage > rankScore.get(i).score) {
				String name = JOptionPane.showInputDialog("Score: " + this.mileage
						+ " Congratulation!!\nPlease Enter your name: ");
				rankScore.add(i,new Player(name, this.mileage));
				if (rankScore.size() > 10)
					rankScore.remove(10);
				diffcultly = 0;
            restart();
				pane.repaint();
				return;
			}
		//If there are less than 10 record 
		if(rankScore.size()<10){
			String name = JOptionPane.showInputDialog("Score: " + this.mileage
					+ " Congratulation!!\nPlease Enter your name: ");
			rankScore.add(new Player(name, this.mileage));
		}else
			JOptionPane.showMessageDialog(null, "Score: " + this.mileage + "You are not on board");
		restart();
		pane.repaint();
	}

	// button events
	@Override
	public void keyPressed(KeyEvent arg0) {
		// TODO Auto-generated method stub
		// press SPACE button to pause game or resume game
		if (arg0.getKeyCode() == KeyEvent.VK_SPACE) {
			if (pause) {
				this.pause = false;
				pane.state.setText("Status: Running");
			} else {
				this.pause = true;
				pane.state.setText("Status: Pause");
			}
		}

		// press UP button to add speed 
		if (arg0.getKeyCode() == KeyEvent.VK_UP) {
           // String chars = "abcdefghijklmnopqrstuvwxyz";
            
            //pane.stat.setTex("Pleasn Enter: "+chars.charAt((int)(Math.random() * 26)));
			if (this.speed != 100) {
				this.speed -= 100;
				pane.speed.setText("Speed: " + (1000/this.speed)*20);
			}
		}

		// when the game is running 
		if (!pause) {
			// press LEFT button move the car left, when it at the left end not move any more
			if (arg0.getKeyCode() == KeyEvent.VK_LEFT) {
				if (car.x > 10)
					car.x -= 30;
				pane.repaint();
			}
      // press RIGHT button move the car left, when it at the right end not move any more
			if (arg0.getKeyCode() == KeyEvent.VK_RIGHT) {
				if (car.x < 130)
					car.x += 30;
				pane.repaint();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub

	}

	// Menu Button
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if (arg0.getSource() == restart)
			restart();
		if (arg0.getSource() == rank){
			this.pause = true;
			pane.state.setText("Status: Pause ");
			new Rank(rankScore);
		}
		if (arg0.getSource() == exit){
			try {
				BufferedWriter os = new BufferedWriter(new FileWriter("rank.dat"));
				for(int i=0;i<rankScore.size();i++)
					os.write(rankScore.get(i).name+ "\r\n"+rankScore.get(i).score+ "\r\n");
				os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.exit(0);
		}
	}
}
class Player{
	public String name;
	public int score;
	public Player(){
		
	}
	public Player(String name,int score){
		this.name = name;
		this.score = score;
	}
}
