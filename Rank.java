import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Rank extends JDialog{

	private JLabel title;
	private JLabel rankLabel[];
	private Image image;
	public Rank(ArrayList<Player> rank) {
		// TODO Auto-generated constructor stub
		setModal(true);
		RankPanel rankPane = new RankPanel(rank);
		this.add(rankPane);
		this.setSize(200, 300);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.show();
	}
	class RankPanel extends JPanel{
		public RankPanel(ArrayList<Player> rank){
			rankLabel = new JLabel[rank.size()];
			image=Toolkit.getDefaultToolkit().getImage("images.jpg");
			title = new JLabel("Best Score");
			title.setBounds(70, 20, 100, 30);
			this.add(title);
			for(int i=0;i<rankLabel.length;i++){
				rankLabel[i] = new JLabel((i+1)+"  "+rank.get(i).name+"----------------------------"+rank.get(i).score);
				rankLabel[i].setOpaque(false);
				rankLabel[i].setBounds(0, 50+i*20, 200, 30);
				rankLabel[i].setOpaque(false);
				this.add(rankLabel[i]);
			}
		}

		@Override
	   protected void paintComponent(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paintComponent(arg0);
		int imWidth=image.getWidth(this); 
		int imHeight=image.getHeight(this); //get the image hights and width 
		int FWidth=getWidth(); 
		int FHeight=getHeight();//get the panel hights adn width
		int x=(FWidth-imWidth)/2; 
		int y=(FHeight-imHeight)/2;//put the image in the middle of the panel
		arg0.drawImage(image,x,y,null);//draw the image
	}
	}

}