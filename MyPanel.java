import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;


import java.awt.Image;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;


import javax.swing.JLabel;
import javax.swing.JPanel;

public class MyPanel extends JPanel{
   
	public JLabel speed;
	public JLabel mileage;
	public ArrayList<Squart> squart; // Store the squart in the pane
	public Squart car; // A red cube represent a car
	public JLabel state;
	public JLabel remark1,remark2,remark3;	//Some word guide player how to control the car
	
   Image image;
   public MyPanel() {
		// TODO Auto-generated constructor stub
		image=Toolkit.getDefaultToolkit().getImage("backForGame.jpg");
      this.setLayout(null);
		this.speed = new JLabel();
		this.speed.setBounds(240, 30, 100, 30);
		this.add(speed);
		this.mileage = new JLabel();
		this.mileage.setBounds(240, 70, 100, 30);
		this.add(mileage);
		this.state = new JLabel();
		this.state.setBounds(240, 110, 100, 30);
		this.add(state);
		this.remark1 = new JLabel("SPACE to pause");
		this.remark1.setBounds(240, 150, 100, 30);
		this.add(remark1);
		this.remark2 = new JLabel("Left to left");
		this.remark2.setBounds(240, 170, 100, 30);
		this.add(remark2);
		this.remark2 = new JLabel("Right to right");
		this.remark2.setBounds(240, 190, 100, 30);
		this.add(remark2);
	}
   @Override
	public void paint(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paint(arg0);
		for(int i=0;i<squart.size();i++)
			squart.get(i).Draw(arg0);
		car.Draw(arg0);
		((Graphics2D)arg0).setStroke(new BasicStroke(5));
		arg0.setColor(Color.blue);
		arg0.drawLine(180, 5, 180, 290);

      }
	@Override
	protected void paintComponent(Graphics arg0) {
		// TODO Auto-generated method stub
		super.paintComponent(arg0);
		int imWidth=image.getWidth(this); 
		int imHeight=image.getHeight(this); //get the image hights and width 
		int FWidth=getWidth(); 
		int FHeight=getHeight();//get the panel hights adn width
		int x=(FWidth-imWidth)/2; 
		int y=(FHeight-imHeight)/2;//put the image in the middle of the panel
		arg0.drawImage(image,x,y,null);//draw the image
	}
}

	


